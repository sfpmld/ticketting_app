import axios from 'axios';

export const buildClient = ({ req }) => {
  if (typeof window === 'undefined') {
    // server side
    return axios.create({
      baseURL: 'http://auth-srv:3000',
      headers: req.headers,
    });
    // browser side
  } else return axios.create({ baseURL: '/' });
};
