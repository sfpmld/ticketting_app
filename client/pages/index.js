import { buildClient } from '../api';

const LandingPage = ({ currentUser }) => {
  return (
    (currentUser && <h1>Your are logged in</h1>) || (
      <h1>Your are not logged in</h1>
    )
  );
};

LandingPage.getInitialProps = async context => {
  const client = buildClient(context);
  const { data } = await client.get('/api/users/currentuser');

  return data;
};

export default LandingPage;
