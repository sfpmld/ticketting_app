const nextEnv = require("next-env");
const dotenvLoad = require("dotenv-load");

dotenvLoad();

const withNextEnv = nextEnv();

module.exports = withNextEnv({
  webpackDevMiddleware: (config) => {
    config.watchOptions.poll = 300;
    return config;
  },
});
