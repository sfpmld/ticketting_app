import { body } from 'express-validator';

export const createOrderValidator = [
  body('ticketId').not().isEmpty().withMessage('TicketId must be provided'),
];
