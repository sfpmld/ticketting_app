import express from 'express';
import 'express-async-errors';
import { json } from 'body-parser';
import cookieSession from 'cookie-session';

import {
  createOrderRouter,
  showOrderRouter,
  listOrderRouter,
  deleteOrderRouter,
  notFoundRouter,
} from './routes';
import { currentUser, errorHandler } from '@sfpmld-gettix/common';
import { NODE_ENV } from './constants';

const app = express();
app.set('trust proxy', true);

app.use(json());
app.use(
  cookieSession({
    signed: false,
    secure: NODE_ENV !== 'test',
  })
);
app.use(currentUser);

app.use(createOrderRouter);
app.use(showOrderRouter);
app.use(listOrderRouter);
app.use(deleteOrderRouter);
app.use(notFoundRouter);

app.use(errorHandler);

export { app };
