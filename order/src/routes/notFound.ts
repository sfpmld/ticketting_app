import express, { Request, Response } from 'express';

import { NotFoundError } from '@sfpmld-gettix/common';

const router = express.Router();

router.all('*', async (_req: Request, _res: Response) => {
  throw new NotFoundError();
});

export { router as notFoundRouter };
