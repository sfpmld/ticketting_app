import { NotFoundError } from '@sfpmld-gettix/common';
import express, { Request, Response } from 'express';
import { Order } from '../models';

const router = express.Router();

router.get('/api/orders/:orderId', async (_req: Request, res: Response) => {
  const order = await Order.find({});

  if (!order) {
    throw new NotFoundError();
  }
  res.status(200).send(order);
});

export { router as showOrderRouter };
