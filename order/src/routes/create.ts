import express, { Request, Response } from 'express';
import { requireAuth, validateRequest } from '@sfpmld-gettix/common';
import { createOrderValidator } from '../middlewares';

const router = express.Router();

router.post(
  '/api/orders',
  requireAuth,
  createOrderValidator,
  validateRequest,
  async (_req: Request, res: Response) => {
    res.status(200).send({});
  }
);

export { router as createOrderRouter };
