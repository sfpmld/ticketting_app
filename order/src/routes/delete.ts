import { NotFoundError } from '@sfpmld-gettix/common';
import express, { Request, Response } from 'express';
import { Order } from '../models';

const router = express.Router();

router.delete('/api/orders', async (_req: Request, res: Response) => {
  res.status(200).send({});
});

export { router as deleteOrderRouter };
