import request from 'supertest';
import { app } from '../../app';

describe('GET Index Order', () => {
  const dummyOrder = {
    title: 'Super Concert',
    price: 20,
  };

  const createOrder = async () => {
    await request(app)
      .post('/api/orders')
      .send(dummyOrder)
      .set('Cookie', global.signin())
      .expect(201);
  };

  it('should fetch a list of orders', async () => {
    await createOrder();
    await createOrder();
    await createOrder();

    const response = await request(app).get('/api/orders').send();

    expect(response.status).toBe(200);
  });
});

