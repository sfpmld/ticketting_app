import mongoose from 'mongoose';

mongoose.set('useCreateIndex', true);

interface TicketAttrs {
  title: string;
  price: number;
}

interface TicketDocument extends mongoose.Document {
  title: string;
  price: number;
}

interface TicketModel extends mongoose.Model<TicketDocument> {
  build(attrs: TicketAttrs): TicketDocument;
}

const TicketSchema = new mongoose.Schema(
  {
    title: {
      type: String,
      required: true,
    },
    price: {
      type: Number,
      required: true,
      min: 0,
    },
  },
  {
    toJSON: {
      transform(_doc, ret) {
        ret.id = ret._id;
        delete ret._id;
      },
    },
  }
);

// build function pattern to let typescript control type for passed User.save()
TicketSchema.statics.build = function (attrs: TicketAttrs) {
  return new Ticket(attrs);
};

const Ticket = mongoose.model<TicketDocument, TicketModel>(
  'Ticket',
  TicketSchema
);

export { Ticket, TicketDocument };
