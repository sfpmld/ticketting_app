import { MongoMemoryServer } from 'mongodb-memory-server';
import mongoose from 'mongoose';
import request, { Response } from 'supertest';
import { app } from '../app';
import { Jwt } from '@sfpmld-gettix/common';

jest.mock('../events/natsWrapper');

let mongo: any;
beforeAll(async () => {
  mongo = new MongoMemoryServer();
  const mongoUri = await mongo.getUri();

  await mongoose.connect(mongoUri, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  });
});

beforeEach(async () => {
  jest.clearAllMocks();
  const collections = await mongoose.connection.db.collections();

  for (let collection of collections) {
    await collection.deleteMany({});
  }
});

afterAll(async () => {
  await mongo.stop();
  await mongoose.connection.close();
});

const enum httpMethod {
  get = 'get',
  post = 'post',
}

declare global {
  namespace NodeJS {
    interface Global {
      fakeUser: {
        email: string;
        password: string;
      };
      requestAPI({
        method,
        path,
        data,
        cookie,
      }: requestAPIOptions): Promise<Response>;
      getRequestAPI(
        path: string,
        cookie: string[] | undefined
      ): Promise<Response>;
      postRequestAPI(
        path: string,
        data: object,
        cookie: string[] | undefined
      ): Promise<Response>;
      signin(): string[];
    }
  }
}

global.fakeUser = {
  email: 'test@test.com',
  password: 'password',
};

interface requestAPIOptions {
  method: httpMethod;
  path: string;
  data?: object | undefined;
  cookie?: string[] | undefined;
}

const requestAPI = async ({
  method,
  path,
  data,
  cookie,
}: requestAPIOptions) => {
  return cookie
    ? data
      ? request(app)[method](path).set('Cookie', cookie).send(data)
      : request(app)[method](path).set('Cookie', cookie)
    : data
    ? request(app)[method](path).send(data)
    : request(app)[method](path);
};

global.signin = () => {
  const dummyUserpayload = {
    id: mongoose.Types.ObjectId().toHexString(),
    email: 'test@test.com',
  };

  // Build jwt token
  const token = Jwt.generateToken(dummyUserpayload);

  const session = { token };

  const sessionJSON = JSON.stringify(session);

  const base64SessionJSON = Buffer.from(sessionJSON).toString('base64');

  return [`express:sess=${base64SessionJSON}`];
};

global.getRequestAPI = async (path: string, cookie: string[] | undefined) => {
  return requestAPI({ method: httpMethod.get, path, cookie });
};

global.postRequestAPI = async (
  path: string,
  data: object,
  cookie: string[] | undefined
) => {
  return requestAPI({ method: httpMethod.post, data, path, cookie });
};
