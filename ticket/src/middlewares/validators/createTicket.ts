import { body } from 'express-validator';

export const createTicketValidator = () => {
  return [
    body('title')
      .trim()
      .notEmpty()
      .isString()
      .withMessage('Title must be a string'),
    body('price')
      .isFloat({ gt: 0 })
      .notEmpty()
      .withMessage('Price must be a number'),
  ];
};
