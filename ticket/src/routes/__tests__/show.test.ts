import request from 'supertest';
import { app } from '../../app';
import mongoose from 'mongoose';

describe('GET Show Ticket', () => {
  const dummyTicket = {
    title: 'Super Concert',
    price: 20,
  };

  const fakeID = new mongoose.Types.ObjectId().toHexString();

  it('should return a 404 if the ticket is not found', async () => {
    const response = await request(app).get(`/api/tickets/${fakeID}`).send({});

    expect(response.status).toBe(404);
  });

  it('should returns the ticket if the ticket is found', async () => {
    const responseNewTicket = await request(app)
      .post('/api/tickets')
      .send(dummyTicket)
      .set('Cookie', global.signin())
      .expect(201);

    const newTicket = await request(app)
      .get(`/api/tickets/${responseNewTicket.body.id}`)
      .send()
      .set('Accept', 'application/json');

    expect(newTicket.status).toBe(200);
  });
});
