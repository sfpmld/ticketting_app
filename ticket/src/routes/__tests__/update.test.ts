import request from 'supertest';
import mongoose from 'mongoose';
import { Subjects } from '@sfpmld-gettix/common';
import { app } from '../../app';
import { natsWrapper } from '../../events';

describe('POST Update Ticket', () => {
  const dummyTicket = {
    title: 'Super Concert',
    price: 20,
  };

  const fakeID = new mongoose.Types.ObjectId().toHexString();

  it('should return 404 if the provided id does not exists', async () => {
    const response = await request(app)
      .put(`/api/tickets/${fakeID}`)
      .send(dummyTicket)
      .set('Cookie', global.signin());

    expect(response.status).toBe(404);
  });

  it('should return a 401 if the user is not authenticated', async () => {
    const response = await request(app)
      .put(`/api/tickets/${fakeID}`)
      .send(dummyTicket);

    expect(response.status).toBe(401);
  });

  it('should return a 401 if the user does not own the ticket', async () => {
    const newTicketResponse = await request(app)
      .post(`/api/tickets`)
      .send(dummyTicket)
      .set('Cookie', global.signin());

    const updatedTicketResponse = await request(app)
      .put(`/api/tickets/${newTicketResponse.body.id}`)
      .send({
        title: 'qsgqg122335s',
        price: 1234,
      })
      .set('Cookie', global.signin());

    expect(updatedTicketResponse.status).toBe(401);
  });

  it('should return a 400 if the user provides an invalid title or price', async () => {
    const cookie = global.signin();

    const newTicketResponse = await request(app)
      .post(`/api/tickets`)
      .send(dummyTicket)
      .set('Cookie', cookie);

    const updatedTicketResponse = await request(app)
      .put(`/api/tickets/${newTicketResponse.body.id}`)
      .send({
        title: '',
        price: 'notGood',
      })
      .set('Cookie', cookie);

    expect(updatedTicketResponse.status).toBe(400);
  });

  it('should update the ticket if provided valid inputs', async () => {
    const cookie = global.signin();
    const wantedTicket = {
      title: 'qsgqg122335s',
      price: 1234,
    };

    const newTicketResponse = await request(app)
      .post(`/api/tickets`)
      .send(dummyTicket)
      .set('Cookie', cookie);

    const updatedTicketResponse = await request(app)
      .put(`/api/tickets/${newTicketResponse.body.id}`)
      .send(wantedTicket)
      .set('Cookie', cookie);

    expect(updatedTicketResponse.body.title).toBe(wantedTicket.title);
    expect(updatedTicketResponse.body.price).toBe(wantedTicket.price);
    expect(updatedTicketResponse.status).toBe(200);
  });

  it(`should emit a ${Subjects.TicketUpdated} event`, async () => {
    const cookie = global.signin();
    const wantedTicket = {
      title: 'qsgqg122335s',
      price: 1234,
    };

    const newTicketResponse = await request(app)
      .post(`/api/tickets`)
      .send(dummyTicket)
      .set('Cookie', cookie);

    const updatedTicketResponse = await request(app)
      .put(`/api/tickets/${newTicketResponse.body.id}`)
      .send(wantedTicket)
      .set('Cookie', cookie);

    expect(updatedTicketResponse.status).toBe(200);
    expect(natsWrapper.client.publish).toHaveBeenCalledTimes(2);
    expect((natsWrapper.client.publish as jest.Mock).mock.calls[0][0]).toEqual(
      Subjects.TicketCreated
    );
    expect((natsWrapper.client.publish as jest.Mock).mock.calls[1][0]).toEqual(
      Subjects.TicketUpdated
    );
  });
});
