import request from 'supertest';
import { app } from '../../app';

describe('GET Index Ticket', () => {
  const dummyTicket = {
    title: 'Super Concert',
    price: 20,
  };

  const createTicket = async () => {
    await request(app)
      .post('/api/tickets')
      .send(dummyTicket)
      .set('Cookie', global.signin())
      .expect(201);
  };

  it('should fetch a list of tickets', async () => {
    await createTicket();
    await createTicket();
    await createTicket();

    const response = await request(app).get('/api/tickets').send();

    expect(response.status).toBe(200);
  });
});

