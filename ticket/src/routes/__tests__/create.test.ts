import request from 'supertest';
import { Subjects } from '@sfpmld-gettix/common';
import { app } from '../../app';
import { natsWrapper } from '../../events';
import { Ticket } from '../../models';

describe('POST Create Ticket', () => {
  it('should have a route handler listening to /api/tickets for post requests', async () => {
    const response = await request(app).post('/api/tickets').send({});

    expect(response.status).not.toEqual(404);
  });

  it('should only be access if the user is signed in', async () => {
    const response = await request(app).post('/api/tickets').send({});

    expect(response.status).toEqual(401);
  });

  it('should return a status other than 401 if the user is signed in', async () => {
    const response = await request(app)
      .post('/api/tickets')
      .send({})
      .set('Cookie', global.signin());

    expect(response.status).not.toBe(401);
  });

  it('should return an error if an invalid title is provided', async () => {
    const response = await request(app)
      .post('/api/tickets')
      .send({
        title: '',
        price: 10,
      })
      .set('Cookie', global.signin());

    expect(response.status).toBe(400);
  });

  it('should return an error if an invalid price is provided', async () => {
    const response = await request(app)
      .post('/api/tickets')
      .send({
        title: 'Ok',
        price: 'bad',
      })
      .set('Cookie', global.signin());

    expect(response.status).toBe(400);
  });

  it('should creates a ticket with valid inputs', async () => {
    const dummyTicket = {
      title: 'OkTitle',
      price: 12,
    };
    const ticket = await Ticket.find({});
    expect(ticket.length).toBe(0);

    const response = await request(app)
      .post('/api/tickets')
      .send(dummyTicket)
      .set('Cookie', global.signin());

    const newTicket = await Ticket.find({});
    expect(newTicket.length).toBe(1);
    expect(response.body).toEqual(
      expect.objectContaining({
        title: dummyTicket.title,
        price: dummyTicket.price,
        userId: expect.any(String),
      })
    );
    expect(response.status).toBe(201);
  });

  it(`should publish a ${Subjects.TicketCreated} event`, async () => {
    const dummyTicket = {
      title: 'OkTitle',
      price: 12,
    };
    const ticket = await Ticket.find({});
    expect(ticket.length).toBe(0);

    const response = await request(app)
      .post('/api/tickets')
      .send(dummyTicket)
      .set('Cookie', global.signin());

    const newTicket = await Ticket.find({});
    expect(newTicket.length).toBe(1);
    expect(response.status).toBe(201);
    expect(natsWrapper.client.publish).toHaveBeenCalledTimes(1);
    expect((natsWrapper.client.publish as jest.Mock).mock.calls[0][0]).toEqual(
      Subjects.TicketCreated
    );
  });
});
