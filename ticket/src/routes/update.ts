import express, { Request, Response } from 'express';
import db from 'mongoose';
import {
  NotFoundError,
  validateRequest,
  requireAuth,
  UnAuthorizedError,
} from '@sfpmld-gettix/common';

import { updateTicketValidator } from '../middlewares';
import { natsWrapper, TicketUpdatedPublisher } from '../events';
import { Ticket } from '../models';

const router = express.Router();

router.put(
  '/api/tickets/:id',
  updateTicketValidator(),
  validateRequest,
  requireAuth,
  async (req: Request, res: Response) => {
    const { id } = req.params;
    const ticket = await Ticket.findById(id);

    if (!ticket) {
      throw new NotFoundError();
    }

    if (ticket.userId !== req.currentUser!.id) {
      throw new UnAuthorizedError();
    }

    const { title, price } = req.body;
    const updatedTicket = ticket.set({
      title,
      price,
    });

    const session = await db.startSession();
    session.startTransaction();

    try {
      await updatedTicket.save();

      const ticketUpdatedPublisher = new TicketUpdatedPublisher(
        natsWrapper.client
      );

      await ticketUpdatedPublisher.publish({
        id: updatedTicket.id,
        title: updatedTicket.title,
        price: updatedTicket.price,
        userId: updatedTicket.userId,
      });

      await session.commitTransaction();

      res.status(200).send(ticket);
    } catch (err) {
      await session.abortTransaction();
    } finally {
      session.endSession();
    }
  }
);

export { router as updateTicketRouter };
