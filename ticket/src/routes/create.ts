import express, { Request, Response } from 'express';
import db from 'mongoose';
import { validateRequest, requireAuth } from '@sfpmld-gettix/common';
import { createTicketValidator } from '../middlewares';
import { Ticket } from '../models';
import { natsWrapper, TicketCreatedPublisher } from '../events';

const router = express.Router();

router.post(
  '/api/tickets',
  requireAuth,
  createTicketValidator(),
  validateRequest,
  async (req: Request, res: Response) => {
    const { title, price } = req.body;

    const ticket = Ticket.build({
      title,
      price,
      userId: req.currentUser!.id,
    });

    const dbSession = await db.startSession();
    dbSession.startTransaction();

    try {
      const newTicket = await ticket.save();

      const ticketCreatedPublisher = new TicketCreatedPublisher(
        natsWrapper.client
      );

      await ticketCreatedPublisher.publish({
        id: ticket.id,
        title: ticket.title,
        price: ticket.price,
        userId: ticket.userId,
      });

      await dbSession.commitTransaction();

      res.status(201).send(newTicket);
    } catch (err) {
      await dbSession.abortTransaction();
    } finally {
      dbSession.endSession();
    }
  }
);

export { router as createTicketRouter };
