import { NotFoundError } from '@sfpmld-gettix/common';
import express, { Request, Response } from 'express';
import { Ticket } from '../models';

const router = express.Router();

router.get('/api/tickets', async (_req: Request, res: Response) => {
  const ticket = await Ticket.find({});

  if (!ticket) {
    throw new NotFoundError();
  }
  res.status(200).send(ticket);
});

export { router as listTicketRouter };
