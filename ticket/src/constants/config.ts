import { getFromEnv } from '@sfpmld-gettix/common';
import { Secret } from 'jsonwebtoken';

export const NODE_ENV = getFromEnv('NODE_ENV');
export const PORT = getFromEnv('PORT');
export const DB_URL = getFromEnv('DB_URL');
export const JWT_SALT = getFromEnv('JWT_SALT') as Secret;
