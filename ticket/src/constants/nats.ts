import { getFromEnv } from '@sfpmld-gettix/common';

export const NATS_CLUSTER_ID = getFromEnv('NATS_CLUSTER_ID');
export const NATS_CLIENT_ID = getFromEnv('NATS_CLIENT_ID');
export const NATS_URL = getFromEnv('NATS_URL');
