import mongoose from 'mongoose';

import { app } from './app';
import { natsWrapper } from './events';
import { DB_URL, PORT } from './constants';
import { NATS_CLUSTER_ID, NATS_CLIENT_ID, NATS_URL } from './constants';

const connectMongoose = async () => {
  return await mongoose.connect(DB_URL, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useCreateIndex: true,
  });
};

const start = async () => {
  try {
    await natsWrapper.connect(NATS_CLUSTER_ID, NATS_CLIENT_ID, NATS_URL);

    natsWrapper.client.on('close', () => {
      console.log('NATS connection closed');
      process.exit();
    });
    process.on('SIGINT', () => natsWrapper.client.close());
    process.on('SIGTERM', () => natsWrapper.client.close());

    await connectMongoose();
    console.log('connected to mongodb database');

    app.listen(PORT, () => {
      console.log(`Listening on port ${PORT}`);
    });
  } catch (err) {
    console.log(err);
  }
};

start();
