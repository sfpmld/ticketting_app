import express from 'express';
import 'express-async-errors';
import { json } from 'body-parser';
import cookieSession from 'cookie-session';

import {
  createTicketRouter,
  showTicketRouter,
  listTicketRouter,
  updateTicketRouter,
  notFoundRouter,
} from './routes';
import { currentUser, errorHandler } from '@sfpmld-gettix/common';
import { NODE_ENV } from './constants';

const app = express();
app.set('trust proxy', true);

app.use(json());
app.use(
  cookieSession({
    signed: false,
    secure: NODE_ENV !== 'test',
  })
);
app.use(currentUser);

app.use(createTicketRouter);
app.use(showTicketRouter);
app.use(listTicketRouter);
app.use(updateTicketRouter);
app.use(notFoundRouter);

app.use(errorHandler);

export { app };
