import { Publisher, Subjects, TicketUpdatedEvent } from '@sfpmld-gettix/common';

export class TicketUpdatedPublisher extends Publisher<TicketUpdatedEvent> {
  readonly subject = Subjects.TicketUpdated;
}
