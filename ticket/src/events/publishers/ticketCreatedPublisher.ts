import { Publisher, Subjects, TicketCreatedEvent } from '@sfpmld-gettix/common';

export class TicketCreatedPublisher extends Publisher<TicketCreatedEvent> {
  readonly subject = Subjects.TicketCreated;
}
