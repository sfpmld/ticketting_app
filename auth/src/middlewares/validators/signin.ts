import { body } from 'express-validator';

export const signinValidator = [
  body('email').isEmail().withMessage('Email must be valid'),
  body('password')
    .trim()
    .isLength({
      min: 4,
      max: 20,
    })
    .notEmpty()
    .withMessage('You must supply a password'),
];
