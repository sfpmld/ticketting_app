import express from 'express';
import 'express-async-errors';
import { json } from 'body-parser';
import cookieSession from 'cookie-session';

import {
  currentUserRouter,
  signinRouter,
  signupRouter,
  signoutRouter,
  notFoundRouter,
} from './routes';
import { errorHandler } from '@sfpmld-gettix/common';
import { NODE_ENV } from './constants';

const app = express();
app.set('trust proxy', true);

app.use(json());
app.use(
  cookieSession({
    signed: false,
    secure: NODE_ENV !== 'test',
  })
);

app.use(currentUserRouter);
app.use(signinRouter);
app.use(signupRouter);
app.use(signoutRouter);

app.use(notFoundRouter);

app.use(errorHandler);

export { app };
