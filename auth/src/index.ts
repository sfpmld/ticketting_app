import mongoose from 'mongoose';

import { app } from './app';
import { DB_URL, PORT } from './constants';

const start = async () => {
  try {
    await mongoose.connect(DB_URL, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useCreateIndex: true,
    });
    console.log('connected to mongodb database');
  } catch (err) {
    console.log(err);
  }
};

app.listen(PORT, () => {
  console.log(`Listening on port ${PORT}`);
});

start();
