import express, { NextFunction, Request, Response } from 'express';

import {
  BadRequestError,
  validateRequest,
  Jwt,
  UserPayloadAttrs,
} from '@sfpmld-gettix/common';

import { signupValidator } from '../middlewares';
import { User, UserAttrs } from '../models';

const router = express.Router();
const createNewUser = async (data: UserAttrs) => {
  const user = User.build(data);
  await user.save();
  return user;
};

router.post(
  '/api/users/signup',
  signupValidator,
  validateRequest,
  async (req: Request, res: Response, _next: NextFunction) => {
    let userTokenPayload: UserPayloadAttrs;
    const { email, password } = req.body;

    const existingUser = await User.findOne({ email });
    if (existingUser) {
      throw new BadRequestError('Email already in use.');
    }

    const user = await createNewUser({ email, password });
    userTokenPayload = {
      id: user.id,
      email: user.email,
    };

    const userToken = Jwt.generateToken(userTokenPayload);

    req.session = {
      token: userToken,
    };

    res.status(201).send(user);
  }
);

export { router as signupRouter };
