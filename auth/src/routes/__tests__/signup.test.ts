describe('Signup Route:', () => {
  it('should return status 201 on successfull signup', async () => {
    const res = await global.postRequestAPI(
      '/api/users/signup',
      global.fakeUser,
      undefined
    );
    expect(res.status).toBe(201);
  });

  it('should return a status 400 when invalid input is given', async () => {
    const res = await global.postRequestAPI(
      '/api/users/signup',
      {
        email: 'testouille',
        password: '1',
      },
      undefined
    );
    expect(res.status).toBe(400);
  });

  it('should return a status 400 when email and password are missing', async () => {
    const resBothMissing = await global.postRequestAPI(
      '/api/users/signup',
      {},
      undefined
    );
    expect(resBothMissing.status).toBe(400);

    const resEmailMissing = await global.postRequestAPI(
      '/api/users/signup',
      {
        password: 'password',
      },
      undefined
    );
    expect(resEmailMissing.status).toBe(400);

    const resPassMissing = await global.postRequestAPI(
      '/api/users/signup',
      {
        email: 'test@test.com',
      },
      undefined
    );
    expect(resPassMissing.status).toBe(400);
  });

  it('should not authorize duplicate email', async () => {
    const signupRes = await global.postRequestAPI(
      '/api/users/signup',
      global.fakeUser,
      undefined
    );
    expect(signupRes.status).toBe(201);

    const res = await global.postRequestAPI(
      '/api/users/signup',
      global.fakeUser,
      undefined
    );
    expect(res.status).toBe(400);
  });

  it('should set a cookie after successfull signup', async () => {
    const res = await global.postRequestAPI(
      '/api/users/signup',
      global.fakeUser,
      undefined
    );
    expect(res.status).toBe(201);
    expect(res.get('Set-Cookie')).toBeDefined();
  });
});
