describe('Signin Route:', () => {
  it("should fails when an email that doesn' exist is supplied", async () => {
    const res = await global.postRequestAPI(
      '/api/users/signin',
      {
        email: 'test@test.com',
        password: 'password',
      },
      undefined
    );
    expect(res.status).toBe(400);
  });

  it('should fails when an incorrect password is supplied', async () => {
    await global.postRequestAPI(
      '/api/users/signup',
      global.fakeUser,
      undefined
    );

    const res = await global.postRequestAPI(
      '/api/users/signin',
      {
        email: 'test@test.com',
        password: 'wrongpassword',
      },
      undefined
    );
    expect(res.status).toBe(400);
  });

  it('should return status 200 and set a cookie on successfull signin', async () => {
    await global.postRequestAPI(
      '/api/users/signup',
      global.fakeUser,
      undefined
    );

    const res = await global.postRequestAPI(
      '/api/users/signin',
      global.fakeUser,
      undefined
    );
    expect(res.status).toBe(200);
    expect(res.get('Set-Cookie')).toBeDefined();
  });
});
