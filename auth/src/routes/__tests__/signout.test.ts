describe('Signout Route:', () => {
  it('should clear cookie after signing out', async () => {
    const signinCookie = await global.signin();

    expect(signinCookie).toBeDefined();

    const res = await global.postRequestAPI(
      '/api/users/signout',
      global.fakeUser,
      undefined
    );
    expect(res.status).toBe(200);
    expect(res.get('Set-Cookie')[0]).toEqual(
      'express:sess=; path=/; expires=Thu, 01 Jan 1970 00:00:00 GMT; httponly'
    );
  });
});
