describe('CurrentUser Route:', () => {
  it('should responds with details about current user', async () => {
    const signinCookie = await global.signin();

    const res = await global.getRequestAPI(
      '/api/users/currentuser',
      signinCookie
    );

    expect(res.status).toBe(200);
    expect(res.body.currentUser).toBeTruthy();
    expect(res.body.currentUser).toEqual(
      expect.objectContaining({
        email: global.fakeUser.email,
      })
    );
  });

  it('should response with null if not authenticated', async () => {
    const res = await global.getRequestAPI('/api/users/currentuser', undefined);

    expect(res.status).toBe(200);
    expect(res.body.currentUser).toBeDefined();
    expect(res.body.currentUser).toEqual(null);
  });
});
