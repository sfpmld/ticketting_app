import express, { Request, Response } from 'express';

import {
  validateRequest,
  BadRequestError,
  Password,
  Jwt,
} from '@sfpmld-gettix/common';
import { signinValidator } from '../middlewares';
import { User } from '../models';

const router = express.Router();

router.post(
  '/api/users/signin',
  signinValidator,
  validateRequest,
  async (req: Request, res: Response) => {
    const { email, password } = req.body;

    const existingUser = await User.findOne({ email });
    if (!existingUser) {
      throw new BadRequestError('Invalid credentials');
    }

    const passwordMatch = await Password.compare(
      existingUser.password,
      password
    );
    if (!passwordMatch) {
      throw new BadRequestError('Invalid credentials');
    }

    const userToken = Jwt.generateToken({
      id: existingUser.id,
      email: existingUser.email,
    });

    req.session = {
      token: userToken,
    };

    res.status(200).send(existingUser);
  }
);

export { router as signinRouter };
